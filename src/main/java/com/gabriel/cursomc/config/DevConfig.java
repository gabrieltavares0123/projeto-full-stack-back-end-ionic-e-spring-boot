package com.gabriel.cursomc.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gabriel.cursomc.services.DBService;
import com.gabriel.cursomc.services.EmailService;
import com.gabriel.cursomc.services.SMTPEmailService;

@Configuration
@Profile("dev")
public class DevConfig {
	
	@Autowired
	private DBService dbService;
	
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String strategy;
	
	@Bean
	public Boolean instantiateDatabase() throws ParseException {
		if (!strategy.equals("create")) {
			return false;
		}else {
			dbService.instantiateTestDatabase();
			return true;
		}
	}
	
	@Bean
	public EmailService emailService() {
		return new SMTPEmailService();
	}
}
