package com.gabriel.cursomc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gabriel.cursomc.domain.Cliente;
import com.gabriel.cursomc.repositories.ClienteRepository;
import com.gabriel.cursomc.security.UserSpringSecurity;

@Service
public class UserDetailServiceImpl implements UserDetailsService{
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// Busca o cliente no banco de dados
		Cliente user = clienteRepository.findByEmail(email);
		
		// Se não encontrar um usuário com o respectivo email, lança uma execessão
		if (user == null) {
			throw new UsernameNotFoundException(email);
		}
		
		//Retorna um novo UserSpringSecurity com todos os dados preenchidos
		return new UserSpringSecurity(user.getId(), user.getEmail(), user.getSenha(), user.getPerfis());
	}

}
