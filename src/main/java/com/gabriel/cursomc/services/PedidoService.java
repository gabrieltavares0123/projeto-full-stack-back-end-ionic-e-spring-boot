package com.gabriel.cursomc.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gabriel.cursomc.domain.Cliente;
import com.gabriel.cursomc.domain.ItemPedido;
import com.gabriel.cursomc.domain.PagamentoComBoleto;
import com.gabriel.cursomc.domain.Pedido;
import com.gabriel.cursomc.domain.enums.EstadoPagamento;
import com.gabriel.cursomc.repositories.ItemPedidoRepository;
import com.gabriel.cursomc.repositories.PagamentoRepository;
import com.gabriel.cursomc.repositories.PedidoRepository;
import com.gabriel.cursomc.security.UserSpringSecurity;
import com.gabriel.cursomc.services.exceptions.AuthorizationException;
import com.gabriel.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	//-------------------------------------
	//-------------Repositories-------------
	//--------------------------------------
	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	
	//-------------------------------------
	//-------------Services-------------
	//--------------------------------------
	@Autowired
	private BoletoService boletoService;

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private EmailService eMailService;
	
	public Pedido findOne(Integer id) {
		Optional<Pedido> cat = pedidoRepository.findById(id);
		return cat.orElseThrow(() -> new ObjectNotFoundException(
				"Objecto não encontrado! ID: " + id + ", Tipo: " + Pedido.class.getName()));
	}

	@Transactional
	public Pedido insert(Pedido pedido) {
		pedido.setId(null);
		pedido.setInstante(new Date());
		
		// Pega o Cliente no banco. Garante que não seja printado no toString null
		pedido.setCliente(clienteService.find(pedido.getCliente().getId()));
		pedido.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		pedido.getPagamento().setPedido(pedido);
		
		if (pedido.getPagamento() instanceof PagamentoComBoleto) {
			PagamentoComBoleto pagto = (PagamentoComBoleto) pedido.getPagamento();
			boletoService.preencherPagamentoComBoleto(pagto, pedido.getInstante());
		}
		
		pedido = pedidoRepository.save(pedido);
		pagamentoRepository.save(pedido.getPagamento());
		
		for (ItemPedido ip : pedido.getItens()) {
			ip.setDesconto(0.0);
			// Pega o Produto no banco. Garante que não seja printado no toString null
			ip.setProduto(produtoService.find(ip.getProduto().getId()));
			ip.setPreco(ip.getProduto().getPreco());
			ip.setPedido(pedido);
		}
		
		itemPedidoRepository.saveAll(pedido.getItens());
		
		//Envia e-mail de confirmação
		//eMailService.sendOrderConfirmationEmail(pedido);
		eMailService.sendOrderConfirmationHTMLEmail(pedido);
		return pedido;
	}
	
	// Busca os pedidos referentes ao cliente logado apenas
	public Page<Pedido> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		// Pega dados do cliente logado
		UserSpringSecurity user = UserService.authenticated();
		if (user == null) {
			throw new AuthorizationException("Aceso negado");
		}
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		// Recupera cliente do banco de dados
		Cliente cliente = clienteService.find(user.getId());
		return pedidoRepository.findByCliente(cliente, pageRequest);
	}
}
