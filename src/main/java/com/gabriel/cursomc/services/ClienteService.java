package com.gabriel.cursomc.services;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gabriel.cursomc.domain.Cidade;
import com.gabriel.cursomc.domain.Cliente;
import com.gabriel.cursomc.domain.Endereco;
import com.gabriel.cursomc.domain.enums.Perfil;
import com.gabriel.cursomc.dto.ClienteDTO;
import com.gabriel.cursomc.dto.ClienteNewDTO;
import com.gabriel.cursomc.repositories.CidadeRepository;
import com.gabriel.cursomc.repositories.ClienteRepository;
import com.gabriel.cursomc.repositories.EnderecoRepository;
import com.gabriel.cursomc.security.UserSpringSecurity;
import com.gabriel.cursomc.services.exceptions.AuthorizationException;
import com.gabriel.cursomc.services.exceptions.DataIntegrityException;
import com.gabriel.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public Cliente find(Integer id) {
		
		UserSpringSecurity user = UserService.authenticated();
		if (user == null || !user.hasHole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}
		
		Optional<Cliente> cat = clienteRepository.findById(id);
		return cat.orElseThrow(
				()-> new ObjectNotFoundException(
						"Objecto não encontrado! ID: " + id + ", Tipo: " + Cliente.class.getName()));
	}
	
	@Transactional
	public Cliente insert(Cliente obj) {
		obj.setId(null);
		obj = clienteRepository.save(obj);
		enderecoRepository.saveAll(obj.getEnderecos());
		return clienteRepository.save(obj);
	}
	
	public Cliente update(Cliente obj) {
		Cliente newObj = find(obj.getId());
		updateData(newObj, obj);
		return clienteRepository.save(newObj);
	}
	
	
	
	public void delete(Integer id) {
		find(id);
		try {
			clienteRepository.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir porque há entidades relacionadas");
		}
	}
	
	public List<Cliente> findAll(){
		return clienteRepository.findAll();
	}
	
	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return clienteRepository.findAll(pageRequest);
	}
	
	public Cliente fromDTO (ClienteDTO dto) {
		return new Cliente(dto.getId(), dto.getNome(), dto.getEmail(), null, null, null);
	}
	
	public Cliente fromDTO (ClienteNewDTO dto) {
		Cliente cli = new Cliente(null, dto.getNome(), dto.getEmail(),dto.getCpfOuCnpj(), dto.getTipo(), bCryptPasswordEncoder.encode(dto.getSenha()));
		// Bastava criar uma cidade e setar o Id, mas prefiro fazer recuperando do banco desse jeito
		Optional<Cidade> cid = cidadeRepository.findById(dto.getCidadeId());
		
		Endereco end = new Endereco(null, dto.getLogradouro(), dto.getNumero(), dto.getComplemento(), 
				dto.getBairro(), dto.getCep(), cli, cid.get());
		
		cli.getEnderecos().add(end);
		cli.getTelefones().add(dto.getTelefone1());
		
		if (dto.getTelefone2() != null)
			cli.getTelefones().add(dto.getTelefone2());
		
		if (dto.getTelefone3() != null)
			cli.getTelefones().add(dto.getTelefone3());
		
		return cli;
	}
	
	private void updateData(Cliente newObj, Cliente obj) {
		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());
	}
}
