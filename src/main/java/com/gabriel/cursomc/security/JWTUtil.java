package com.gabriel.cursomc.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtil {

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.expiration}")
	private Long expiration;

	public String generateToken(String username) {
		return Jwts.builder().setSubject(username).setExpiration(new Date(System.currentTimeMillis() + expiration))
				.signWith(SignatureAlgorithm.HS512, secret.getBytes()).compact();
	}

	/* Verifica se um token é válido */
	public Boolean validToken(String token) {
		// Armazena as reinvindicações do token. Nesse caso são usuário e tempo de
		// expiração
		Claims claims = getClaims(token);

		// Verifica se as reinvindicações não são null
		if (claims != null) {
			String username = claims.getSubject();
			// Data de expiração
			Date expirationDate = claims.getExpiration();
			// Data de agora
			Date now = new Date(System.currentTimeMillis());

			// Verifica se o token não está expirado
			if (username != null && expirationDate != null && now.before(expirationDate)) {
				return true;
			}
		}

		return false;
	}

	/* Recupera os Claims a partir de um token */
	private Claims getClaims(String token) {
		try {
			return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			return null;
		}
	}

	public String getUsername(String token) {
		// Armazena as reinvindicações do token. Nesse caso são usuário e tempo de
		// expiração
		Claims claims = getClaims(token);

		// Verifica se as reinvindicações não são null
		if (claims != null) {
			return claims.getSubject();
		}
		
		return null;
	}
}
